" Vim syntax file
" Language: fsm
" Maintainer: Matvey Larionov
" Latest Revision: 11 November 2013

if exists("b:current_syntax")
  finish
endif

" Nodes
syn match fsmNode "\<\u\w*"

" Operators
syn match fsmOp "\."
syn match fsmOp "->"

" Comments
syn match fsmTodo "[tT][oO][dD][oO]" contained
syn match fsmLineComment "--.*" contains=fsmTodo


let b:current_syntax= "fsm"

hi def link fsmNode        Keyword
hi def link fsmOp          Special
hi def link fsmTodo        Todo
hi def link fsmLineComment Comment
