#ifndef fsm_designer_graph_h
#define fsm_designer_graph_h

#include <algorithm>
#include <list>
#include <utility>

namespace fsm_designer {

template <typename VType, typename EType>
class Graph {
public:

  class Vertex {
  public:
    typedef std::pair<const Vertex*, EType> Edge;
    typedef std::list<Edge> Edges;
  public:
    explicit Vertex (const VType& v_value) : m_value(v_value) {}
    void AddEdge (const Vertex* other, const EType& e_value);
    void RemoveEdge (const Vertex* other, const EType& e_value);
    const EType& value () const { return m_value; }
    EType* get_value () { return &m_value; }
    const Edges& edges () const { return m_edges; }
    Edges* get_edges () { return &m_edges; }
    bool operator == (const VType& value) const { return m_value == value; }
  private:
    VType m_value;
    Edges m_edges;
  };  // class Vertex

  typedef std::list<Vertex> Vertices;

public:
  Graph () {}
  Vertex& AddVertex (const VType& value);
  const Vertices& vertices () const { return m_vertices; }
  Vertices* get_vertices () { return &m_vertices; }
  Vertex* get_vertex (const VType& value);
  const Vertex* get_vertex (const VType& value) const;

private:
  Vertices m_vertices;
};  // class Graph

template <typename VType, typename EType>
void Graph<VType, EType>::Vertex::AddEdge (const Vertex* other,
                                           const EType& e_value) {
  m_edges.push_back(Edge(other, e_value));
}

template <typename VType, typename EType>
void Graph<VType, EType>::Vertex::RemoveEdge (const Vertex* other,
                                              const EType& e_value) {
  m_edges.remove(Edge(other, e_value));
}

template <typename VType, typename EType>
typename Graph<VType, EType>::Vertex& Graph<VType, EType>::AddVertex (
    const VType& value) {
  m_vertices.push_back(Vertex(value));
  return *(m_vertices.rbegin());
}

template <typename VType, typename EType>
typename Graph<VType, EType>::Vertex* Graph<VType, EType>::get_vertex (
    const VType& value) {
  typename Vertices::iterator res = std::find(m_vertices.begin(),
                                              m_vertices.end(),
                                              value);

  if (res == m_vertices.end())
    return NULL;
  return &(*(res));
}

template <typename VType, typename EType>
const typename Graph<VType, EType>::Vertex* Graph<VType, EType>::get_vertex (
    const VType& value) const {
  typename Vertices::const_iterator res = std::find(m_vertices.begin(),
                                                    m_vertices.end(),
                                                    value);

  if (res == m_vertices.end())
    return NULL;
  return &(*(res));
}

}  // fsm_designer

#endif  // fsm_designer_graph_h
