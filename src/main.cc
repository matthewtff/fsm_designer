#include "validator.h"

#include <iostream>
#include <fstream>

int ShowUsing () {
  std::cout << "Using: fsm_designer <file_name>" << std::endl;
  return 1;
}

int BadFile (const std::string& file_name) {
  std::cout << "Can't open file '" << file_name << "'" << std::endl;
  return 1;
}

int main(int argc, char* argv[]) {
  if (argc < 2)
    return ShowUsing();

  std::ifstream in(argv[1]);
  if (!in.good())
    return BadFile(argv[1]);

  fsm_designer::Validator validator(in);
  if (validator.error()) {
    std::cerr << "Validation error: " << validator.get_error() << std::endl;
    return 2;
  } else {
    std::cout << "Grats! Validation successfull!" << std::endl;
    validator.ShortestPaths();
  }
  return 0;
}
