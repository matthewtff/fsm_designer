#include "validator.h"

#include <algorithm>
#include <iostream>
#include <istream>
#include <iterator>
#include <sstream>
#include <string>

namespace {

// Comment lines start from two dashes.
bool IsComment (const std::string& line) {
  if (line.length() < 2)
    return false;

  return line[0] == '-' && line[1] == '-';
}

void StripComments (std::istream& input, std::stringstream& stream) {
  std::string line;
  while (std::getline(input, line))
    if (!IsComment(line))
      stream << line << std::endl;
}

template <typename T>
T PopFront(std::list<T>& l) {
  T element = l.front();
  l.pop_front();
  return element;
}

}  // namespace

namespace fsm_designer {

const char Validator::m_start[] = "start";
const char Validator::m_stop[] = "stop";

Validator::Validator (std::istream& input) : m_current_node(NULL) {
  std::stringstream no_comments;
  StripComments(input, no_comments);
  Tokens tokens;
  std::copy(std::istream_iterator<std::string>(no_comments),
            std::istream_iterator<std::string>(),
            std::back_inserter<Tokens>(tokens));

  while (Process(tokens)) {}

  if (!m_error.empty())
    return;

  if (!tokens.empty())
    m_error = "Can't consume all data";
  else
    RunChecks();
}

bool Validator::Process (Tokens& tokens) {
  if (tokens.size() < 2)
    return false;

  std::string first = PopFront(tokens);
  std::string second = PopFront(tokens);
  if (first == "->")
    return AddEvent(second);
  else if (first == ".")
    return StartNode(second);
  else
    return AddEdge(first, second);
}

Validator::FSM::Vertex* Validator::FindOrAddNode (const std::string& node) {
  FSM::Vertex* vertex = m_fsm.get_vertex(node);
  if (!vertex)
    vertex = &(m_fsm.AddVertex(node));
  return vertex;
}

bool Validator::EventExists (const std::string& event) {
  return m_events.end() != std::find(m_events.begin(),
                                     m_events.end(),
                                     event);
}

bool Validator::AddEvent (const std::string& event) {
  if (EventExists(event)) {
    m_error = "Duplicate event: " + event;
    return false;
  }
  m_events.push_back(event);
  m_unused_events.push_back(event);
  return true;
}

bool Validator::StartNode (const std::string& node) {
  m_current_node = FindOrAddNode(node);
  if (!m_current_node) {
    m_error = "Internal error on node: " + node;
    return false;
  }
  return true;
}

bool Validator::AddEdge (const std::string& event, const std::string& node) {
  if (!m_current_node) {
    m_error = "No current node, can't make edge to " + node + " on " + event;
    return false;
  }
  if (!EventExists(event)) {
    m_error = "Unknown event: " + event;
    return false;
  } else {
    m_unused_events.remove(event);
  }
  FSM::Vertex* to_node = FindOrAddNode(node);
  if (!to_node) {
    m_error = "Internal error on node: " + node;
    return false;
  }
  m_current_node->AddEdge(to_node, event);
  return true;
}

void Validator::RunChecks () {
  if (!m_unused_events.empty()) {
    m_error = "Unused events: " + m_unused_events.front();
    return;
  }

  if (!m_fsm.get_vertex(m_start)) {
    m_error = "No 'start' state";
    return;
  }

  if (!m_fsm.get_vertex(m_stop)) {
    m_error = "No 'stop' state";
    return;
  }

  if (!StopIsReachable()) {
    m_error = "'stop' is not reachable from 'start'";
    return;
  }
}

bool Validator::StopIsReachable () const {
  const FSM::Vertex* start = m_fsm.get_vertex(m_start);
  if (!start)
    return false;

  // Use BFS with opened and closed queues to prevent cycles trap.
  typedef std::list<const FSM::Vertex*> VertexQueue;
  VertexQueue open_queue;
  open_queue.push_back(start);
  VertexQueue closed_queue;

  while (!open_queue.empty()) {
    const FSM::Vertex* vertex = PopFront(open_queue);
    if (vertex->value() == m_stop)
      return true;

    const FSM::Vertex::Edges& edges = vertex->edges();
    for (FSM::Vertex::Edges::const_iterator edge = edges.begin();
         edge != edges.end(); ++edge) {
      if (closed_queue.end() == std::find(closed_queue.begin(),
                                          closed_queue.end(),
                                          edge->first)) {
        open_queue.push_back(edge->first);
        closed_queue.push_back(edge->first);
      }
    }
  }

  return false;
}

namespace {

using fsm_designer::Validator;

struct Transition {
  std::string state;
  std::string event;
};  // struct Transition

typedef std::list<Transition> Path;
typedef std::pair<const Validator::FSM::Vertex*, Path> VertexData;
typedef std::list<const Validator::FSM::Vertex*> ClosedQueue;

void PrintPath(const Path& path) {
  for (Path::const_iterator state = path.begin();
       state != path.end(); ++state) {
    if (state != path.begin())
      std::cout << " --" << state->event << "-> " << state->state;
    else
      std::cout << state->state;
  }
  std::cout << std::endl;
}

void DFS (const VertexData& data, const ClosedQueue& closed_queue) {
  if (data.first->value() == Validator::m_stop) {
    PrintPath(data.second);
    return;
  }

  const Validator::FSM::Vertex::Edges& edges = data.first->edges();
  for (Validator::FSM::Vertex::Edges::const_iterator edge = edges.begin();
       edge != edges.end(); ++edge) {
    if (closed_queue.end() == std::find(closed_queue.begin(),
                                        closed_queue.end(),
                                        edge->first)) {

      Transition transition = {edge->first->value(), edge->second};
      Path new_path = data.second;
      new_path.push_back(transition);
      ClosedQueue new_closed_queue = closed_queue;
      new_closed_queue.push_back(edge->first);
      DFS(VertexData(edge->first, new_path), new_closed_queue);
    }
  }
}

void DoDFS (const Validator::FSM::Vertex* start) {
  // Use BFS with opened and closed queues to prevent cycles trap.
  Transition start_transition = {start->value(), ""};
  Path start_path;
  start_path.push_back(start_transition);
  VertexData start_data(start, start_path);
  ClosedQueue closed_queue;
  closed_queue.push_back(start);

  DFS(start_data, closed_queue);
}

}  // namespace

void Validator::ShortestPaths () const {
  const FSM::Vertex* start = m_fsm.get_vertex(m_start);
  if (!start)
    return;
  DoDFS(start);
}

}  // namespace fsm_designer
