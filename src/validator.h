#ifndef fsm_designer_validator_h
#define fsm_designer_validator_h

#include <istream>
#include <list>
#include <string>
#include <vector>

#include "graph.h"

namespace fsm_designer {

class Validator {
public:
  typedef std::string State;
  typedef std::string Event;
  typedef std::list<std::string> Tokens;
  typedef Graph<State, Event> FSM;

public:
  Validator (std::istream& data);
  bool error () const { return !m_error.empty(); }
  std::string get_error () const { return m_error; }

  void ShortestPaths () const;

public:
  static const char m_start[];
  static const char m_stop[];

private:
  bool Process (Tokens& tokens);
  FSM::Vertex* FindOrAddNode (const std::string& node);
  bool EventExists (const std::string& event);
  bool AddEvent (const std::string& event);
  bool StartNode (const std::string& node);
  bool AddEdge (const std::string& event, const std::string& node);

  void RunChecks ();

  bool StopIsReachable () const;

private:

  std::vector<Event> m_events;
  std::list<Event> m_unused_events;
  FSM m_fsm;
  FSM::Vertex* m_current_node;
  std::string m_error;
};  // class Validator

}  // namespace fsm_designer

#endif  // fsm_designer_validator_h
